<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Note specific routes
 */
Route::any('/', 'NoteTrackerController@index');
Route::post('create', 'NoteTrackerController@createNote');
Route::put('update', 'NoteTrackerController@updateNote');
Route::patch('trash', 'NoteTrackerController@trashNote');
Route::patch('color', 'NoteTrackerController@colorNote');
Route::delete('remove', 'NoteTrackerController@removeNote');
/**
 * Categories specific routes
 */
Route::get('category/available', 'CategoryController@getAll');
Route::any('category/list', 'CategoryController@byCategory');