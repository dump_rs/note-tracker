<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ColorNote extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'note.id' => 'required|integer',
            'note.category_id' => 'required|integer',
            'note.color' => 'required|size:7',
        ];
    }
}
