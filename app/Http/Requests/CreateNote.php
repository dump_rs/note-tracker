<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CreateNote extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if($request->has('note.category_id')) {
            if ($request->input('note.category_id') == 0)
                return [
                    'note.title' => 'required|min:1|max:255',
                    'note.content' => 'required'
                ];

            if ($request->input('note.category_id') == 1)
                return [
                    'note.image' => 'required',
                ];

            if ($request->input('note.category_id') == 2)
                return [
                    'note.content' => 'required|url'
                ];
        }
        return ['note.category_id' => 'required|integer'];
    }
}
