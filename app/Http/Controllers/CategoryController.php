<?php

namespace App\Http\Controllers;


use App\Services\Category\CategoryService;
use Illuminate\Http\Request;

class CategoryController
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function getAll()
    {
        return response()->json($this->categoryService->getAll());
    }

    public function byCategory(Request $request)
    {
        return response()->json($this->categoryService->getCategoryNotes($request));
    }
}