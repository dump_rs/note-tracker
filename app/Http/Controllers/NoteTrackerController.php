<?php

namespace App\Http\Controllers;

use App\Http\Requests\ColorNote;
use App\Http\Requests\CreateNote;
use App\Http\Requests\ListNotes;
use App\Http\Requests\NoteWithCategoryAndId;
use App\Http\Requests\UpdateNote;
use App\Repositories\CategoryRepository;
use App\Services\Note\NoteService;
use Illuminate\Routing\Controller as BaseController;
use App\Repositories\TextNoteRepository;

class NoteTrackerController extends BaseController
{

    protected $textNote;
    protected $noteService;

    public function __construct(TextNoteRepository $textNoteRepository, CategoryRepository $categoryRepository, NoteService $noteService)
    {
        $this->textNote = $textNoteRepository;
        $this->noteService = $noteService;
    }

    public function index(ListNotes $request)
    {
        return response()->json($this->noteService->getAll($request));
    }

    public function createNote(CreateNote $request)
    {
        return response()->json($this->noteService->createNewNote($request));
    }

    public function updateNote(UpdateNote $request)
    {
        return response()->json($this->noteService->updateNote($request));
    }

    public function trashNote(NoteWithCategoryAndId $request)
    {
        return response()->json($this->noteService->trashNote($request));
    }

    public function removeNote(NoteWithCategoryAndId $request)
    {
        return response()->json($this->noteService->removeNote($request));
    }

    public function colorNote(ColorNote $request)
    {
        return response()->json($this->noteService->changeNoteColor($request));
    }
}
