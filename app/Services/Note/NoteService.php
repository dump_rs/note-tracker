<?php

namespace App\Services\Note;

use App\Repositories\CategoryRepository;
use App\Repositories\ImageNoteRepository;
use App\Repositories\TextNoteRepository;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class NoteService
{
    /**
     * Auto-loaded note repository
     *
     * @var \App\Repositories\*
     */
    protected $note;

    /**
     * @var TextNoteRepository
     */
    protected $textNote;

    /**
     * @var CategoryRepository
     */
    protected $category;

    /**
     * Map database internal id -> matching repo
     *
     * @var array
     */
    protected $category_map;

    public function __construct(Request $request, TextNoteRepository $noteRepository, CategoryRepository $categoryRepository)
    {
        $this->category_map = [];
        $this->textNote = $noteRepository;
        $this->category = $categoryRepository;
        $this->note = $this->getNoteInstance($request);
    }

    /**
     * Load all notes, offset by page number and per page notes(env)
     *
     * @param Request $request
     * @return array
     */
    public function getAll(Request $request)
    {
        return $this->textNote->getAllNotes($request->input('note.page'));
    }

    /**
     * Save new note
     *
     * @param Request $request
     * @return mixed
     */
    public function createNewNote(Request $request)
    {
        if($this->note instanceof ImageNoteRepository){
            return $this->note->create(['image' => $this->handleNoteImage($request)]);
        }else{
            return $this->note->create($request->input('note'));
        }
    }

    /**
     * Update note, for image notes check if request has indicate for base64 compression
     *
     * @param Request $request
     * @return mixed
     */
    public function updateNote(Request $request)
    {
        if($this->note instanceof ImageNoteRepository){
            return $this->note->update($request->input('note.id'), ['image' => $this->handleNoteImage($request)]);
        }else{
            return $this->note->update($request->input('note.id'), $request->input('note'));
        }
    }

    /**
     * Decompress, decode & save post image
     *
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    private function handleNoteImage(Request $request) : string
    {
        $image = $request->input('note.image');
        try {
            if ($request->input('note.compressed')) {
                $image = gzdecode($image);
            }
            if ($this->base64ImageSize($decoded_image = base64_decode($image)) <= env('MAX_IMAGE_B_SIZE', 1))
                $image_name = sha1(str_random());
            $note_image = Image::make($decoded_image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $note_image->save(public_path('images') . '/' . $image_name . '.jpg');
        }catch (\Exception $exception){
            //throw new  \Exception('Error processing image.');
            throw $exception;
        }
        return $image_name;
    }

    /**
     * Remove/destroy note
     *
     * @param Request $request
     * @return mixed
     */
    public function removeNote(Request $request)
    {
        return $this->note->destroy($request->input('note.id'));
    }

    /**
     * Mark note as trash & set appropriate state
     *
     * @param Request $request
     * @return mixed
     */
    public function trashNote(Request $request)
    {
        return $this->note->trash($request->input('note.id'));
    }

    /**
     * Change note color to selected, must be in defined range
     *
     * @param Request $request
     * @return mixed
     */
    public function changeNoteColor(Request $request)
    {
        return $this->note->changeColor($request->input('note.id'), $request->input('note.color'));
    }

    /**
     * Return loaded repo based on category_id from request
     *
     * @param Request $request
     * @return mixed
     */
    private function getNoteInstance(Request $request)
    {
        return $this->getResolveInstance($request->input('note.category_id'));
    }

    /**
     * Autoload available repos from persistent layer,
     * _category_ signature must match namespace and class name
     * files in \app\Repositories
     * If category_id is null, seams we dont need to create anything,
     * return empty object as placeholder
     *
     * @param int|null $category
     * @return mixed
     * @throws \Exception
     */
    private function getResolveInstance($category)
    {
        if(!is_null($category)) {
            if (empty($this->category_map)) {
                $load_categories = $this->category->getAll();
                if (count($load_categories)) {
                    foreach ($load_categories as $categories) {
                        $this->category_map[$categories->id] = [
                            'repository' => (new \ReflectionObject($this->category))->getNamespaceName() . '\\' . $categories->signature];
                    }
                }
            }
            if (array_key_exists($category, $this->category_map)) {
                $note_instance = $this->category_map[$category];
                $note_instance = new $note_instance['repository'];
                $note_instance->setCategoryId($category);
                return $note_instance;
            }
            throw new \Exception('Category doesn\'t exist.');
        }
        return new \stdClass();
    }

    /**
     * Calculate ~size of base64 stream
     *
     * @param string $image
     * @return int
     */
    private function base64ImageSize(string $image){
        return (int) (strlen(rtrim($image, '=')) * 3 / 4);
    }
}