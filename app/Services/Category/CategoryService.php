<?php

namespace App\Services\Category;

use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryService
{
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getAll()
    {
        return $this->categoryRepository->getAll();
    }

    public function getCategoryNotes(Request $request)
    {
        return $this->categoryRepository->getCategoryNotes($request->input('category.id'));
    }
}