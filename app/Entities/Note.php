<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table='notes';

    public function category()
    {
        return $this->belongsTo('App\Entities\Category');
    }
}