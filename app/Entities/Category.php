<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $hidden = ['signature'];

    public function notes()
    {
        return $this->hasMany('App\Entities\Note');
    }
}