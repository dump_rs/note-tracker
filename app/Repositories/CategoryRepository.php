<?php

namespace App\Repositories;

use App\Entities\Category;

class CategoryRepository
{
    /**
     * Eloquent instance
     *
     * @var Category
     */
    protected $model;

    public function __construct()
    {
        $this->model = new Category();
    }

    /**
     * Get all available categories from db
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get all notes from specific category
     *
     * @param int $category_id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCategoryNotes(int $category_id)
    {
        return $this->model->with('notes')->where('id', $category_id)->get();
    }
}