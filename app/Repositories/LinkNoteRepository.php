<?php

namespace App\Repositories;

use App\Entities\Note;

class LinkNoteRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Note();
    }

    public function create(array $payload)
    {
        $this->model->content = $payload['content'];
        $this->model->category_id = $this->category_id;

        $this->model->saveOrFail();
        return $this->model;
    }

    public function update(int $id, array $payload)
    {
        $update_model = $this->model->where('category_id', $this->category_id)->where('id', $id)->firstOrFail();

        $update_model->content = $payload['content'];
        $update_model->save();

        return $update_model;
    }
}