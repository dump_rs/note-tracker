<?php

namespace App\Repositories;

use App\Entities\Note;
use Mockery\Exception;

abstract class BaseRepository
{
    /**
     * Eloquent instance
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Inject category ID
     *
     * @var int
     */
    public $category_id;

    /**
     * Available colors for notes
     *
     * @var array
     */
    protected $color_range = ['#ffffff', '#fcc5c0', '#b7e9ff', '#cfd8dc'];

    /**
     * @param int $category_id
     */
    public function setCategoryId(int $category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * Create model
     *
     * @param array $payload
     * @return mixed
     */
    abstract public function create(array $payload);

    /**
     * Update model
     *
     * @param int $id
     * @param array $payload
     * @return mixed
     */
    abstract public function update(int $id, array $payload);

    /**
     * Fetch record by internal/DB ID
     *
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function get(int $id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Return simple pagination of all notes,
     * use generic \App\Entities\Note as a gate to all notes
     *
     * @param int $page
     * @return array
     */
    public function getAllNotes(int $page = 1)
    {
        $notes_model = new Note();
        $page = abs($page);
        $notes = [];
        $per_page = env('NOTES_PER_PAGE', 10);
        $max_pages = (int) ceil($notes_model->where('state', 1)->count() / $per_page);
        if($page > 0) {
            if ($page <= $max_pages) {
                $notes = $notes_model->where('state', 1)->skip(($page - 1) * $per_page)->take($per_page)->get();
            }
        }
        return [
            'notes' => $notes,
            'page' => $page,
            'count' => count($notes),
            'total_pages' => $max_pages
        ];
    }

    /**
     * Fetch record by public ID
     *
     * @param string $public_id
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function getByPublicId(string $public_id)
    {
        return $this->model->where('public_id', $public_id)->first();
    }

    /**
     * Mark note as trash
     *
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model|\Exception
     */
    public function trash(int $id)
    {
        $target_note = $this->model->findOrFail($id);
        $target_note->state = 0;
        $target_note->save();
        return $target_note;
    }

    /**
     * Set note color
     *
     * @param int $id
     * @param string $color
     * @return mixed
     * @throws \Exception
     */
    public function changeColor(int $id, string $color)
    {
        if(in_array($color, $this->color_range)) {
            $target_note = $this->model->findOrFail($id);
            $target_note->color = $color;
            $target_note->save();
            return $target_note;
        }
        throw new \Exception('Selected color isn\'t supported.');
    }

    /**
     * Destroy record by internal ID
     *
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $target_note = $this->get($id);
        if(!$target_note->state) {
            $this->model::destroy($id);
            return [];
        }
        throw new Exception('Note is not marked as trash.');
    }
}