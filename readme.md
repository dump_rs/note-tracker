##Note Tracker API

##### Requirements

- PHP ^7.1.3
- Sqlite | MySQL
- php-gd
- phpunit
- docker (`~/laradock/docker-compose nginx workspace`) | vagrant (`Laravel or Symfony box`) | *nix env

#####For manual deployment:

- composer install
- php artisan migrate && php artisan db:seed
- run php server or so (specify APP_URL in .env)
- phpunit
- `or` use task list in `deploy.sh` 

#####How/where/what

- All endpoints are under `project.url/api/`
- Request examples are part of tests `/tests/Feature/*`
- Notes images are under `/public/images/`
- Seeded database is `/database/notes.sqlite`


