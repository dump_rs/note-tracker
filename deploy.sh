#!/bin/bash

#Assume is available from /bin
composer install

printf "\nRunning migrations...\n"
php artisan migrate
php artisan migrate:refresh
printf "\nSeeding DB, this can take minute or two(collecting random images from Google & ~~NSFW)...\n"
php artisan db:seed
printf "\nDone seeding.\n"
composer dumpautoload
printf "\nStarting local server on :9009\n"
#nohup php artisan serve --port=9009 &
printf "\nWith PID\n"
ps -ef | grep "$PWD/server.php"
printf "\nRunning tests...\n"
phpunit
printf "\nDone.\n"