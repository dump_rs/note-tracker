<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['id' => 0, 'name' => 'Notes', 'signature' => 'TextNoteRepository'],
            ['id' => 1, 'name' => 'Images', 'signature' => 'ImageNoteRepository'],
            ['id' => 2, 'name' => 'Links', 'signature' => 'LinkNoteRepository']
        ]);
    }
}
