<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Buchin\GoogleImageGrabber\GoogleImageGrabber;
use Intervention\Image\ImageManagerStatic as Image;

class NotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $color_range = ['#ffffff', '#fcc5c0', '#b7e9ff', '#cfd8dc'];

        $faker = \Faker\Factory::create();
        $data_set = [];
        //Generate TextNotes data set
        foreach (range(0, 14) as $i){
            $data_set[] = [
                'title' => $faker->text(25),
                'content' => $faker->text(100),
                'category_id' => 0,
                'color' => $color_range[rand(0, count($color_range) -1)]
            ];
        }

        /**
        * Generate ImageNotes data set
        * Use imagick or gd for Intervention\Image driver
        **/
        Image::configure(array('driver' => 'gd'));
        //Google sometimes returns rubbish data, so brute force it
        $images_collected = 0;
        foreach (range(0, 100) as $i){
            /**
             * Get random image from google imgs
             * @todo Check if Google response is changed
             */
            if($images_collected < 15) {
                try {
                    $random_image = GoogleImageGrabber::grab($faker->text(7));

                    $faker_image = Image::make($random_image[0]['url'])->resize(300, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $faker_image_name = sha1($i . str_random());
                    $faker_image->save(public_path('images') . '/' . $faker_image_name . '.jpg');

                    $data_set[] = [
                        'image' => $faker_image_name,
                        'category_id' => 1,
                        'color' => $color_range[rand(0, count($color_range) -1)]
                    ];
                    $images_collected++;
                }catch (\Exception $exception){}
            }
        }
        //Generate LinkNotes data set
        foreach (range(0, 14) as $i){
            $data_set[] = [

                'content' => $faker->url,
                'category_id' => 2,
                'color' => $color_range[rand(0, count($color_range) -1)]
            ];
        }

        shuffle($data_set);
        foreach ($data_set as $record){
            DB::table('notes')->insert($record);
        }
    }
}
