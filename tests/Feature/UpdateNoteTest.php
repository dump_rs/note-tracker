<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateNoteTest extends TestCase
{
    /**
     * Test update note
     *
     * @return void
     */
    public function testUpdateNote()
    {
        $response_create = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'POST',
                '/api/create',
                [
                    'note' => [
                        'title' => 'Test Update Title',
                        'content' => 'Test Update Content',
                        'category_id' => 0
                    ]
                ]
            );

        $response_create
            ->assertStatus(200)
            ->assertJson([
                'title' => true
            ]);

        $created_note = json_decode($response_create->getContent());

        $response = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'PUT',
                '/api/update',
                [
                    'note' => [
                        'id' => $created_note->id,
                        'title' => '+ Test Update Title',
                        'content' => '+ Test Update Content',
                        'category_id' => 0
                    ]
                ]
            );

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'title' => '+ Test Update Title'
            ]);

    }
}
