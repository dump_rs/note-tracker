<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TrashNoteTest extends TestCase
{
    /**
     * Test trash note
     *
     * @return void
     */
    public function testTrashNote()
    {
        $response_create = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'POST',
                '/api/create',
                [
                    'note' => [
                        'title' => 'Test Trash Title',
                        'content' => 'Test Trash Content',
                        'category_id' => 0
                    ]
                ]
            );

        $response_create
            ->assertStatus(200)
            ->assertJson([
                'title' => true
            ]);

        $created_note = json_decode($response_create->getContent());

        $response = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'PATCH',
                '/api/trash',
                [
                    'note' => [
                        'id' => $created_note->id,
                        'category_id' => 0
                    ]
                ]
            );

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'state' => 0
            ]);

    }
}
