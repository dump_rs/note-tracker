<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ImageNoteCreateTest extends TestCase
{
    /**
     * Test if image note is created
     *
     * @return void
     */
    public function testCreateImageNote()
    {
        $response = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'POST',
                '/api/create',
                [
                    'note' => [
                        'image' => base64_encode(file_get_contents(public_path() . '/sample.jpg')),
                        'category_id' => 1
                    ]
                ]
            );

        $response
            ->assertStatus(200)
            ->assertJson([
                'image' => true
            ]);
    }
}
