<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ListCategoriesTest extends TestCase
{
    /**
     * Test list categories
     *
     * @return void
     */
    public function testCategoryLoad()
    {
        $response = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'GET',
                '/api/category/available'
            );

        $response
            ->assertStatus(200)
            ->assertJsonCount(3);
    }
}
