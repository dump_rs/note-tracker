<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LinkNoteCreateTest extends TestCase
{
    /**
     * Test if link note is created
     *
     * @return void
     */
    public function testCreateLinkNote()
    {
        $response = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'POST',
                '/api/create',
                [
                    'note' => [
                        'content' => 'http://test.link',
                        'category_id' => 2
                    ]
                ]
            );

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'content' => 'http://test.link'
            ]);
    }
}
