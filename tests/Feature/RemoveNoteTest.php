<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RemoveNoteTest extends TestCase
{
    /**
     * Test remove note
     *
     * @return void
     */
    public function testRemoveNote()
    {
        $response_create = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'POST',
                '/api/create',
                [
                    'note' => [
                        'title' => 'Test Remove Title',
                        'content' => 'Test Remove Content',
                        'category_id' => 0
                    ]
                ]
            );

        $response_create
            ->assertStatus(200)
            ->assertJson([
                'title' => true
            ]);

        $created_note = json_decode($response_create->getContent());

        $response_trash = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'PATCH',
                '/api/trash',
                [
                    'note' => [
                        'id' => $created_note->id,
                        'category_id' => 0
                    ]
                ]
            );

        $response_trash
            ->assertStatus(200)
            ->assertJsonFragment([
                'state' => 0
            ]);

        $response = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'DELETE',
                '/api/remove',
                [
                    'note' => [
                        'id' => $created_note->id,
                        'category_id' => 0
                    ]
                ]
            );
        $response
            ->assertStatus(200)
            ->assertExactJson([]);

    }
}
