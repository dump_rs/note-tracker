<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoadFirstPageNoteCreateTest extends TestCase
{
    /**
     * Test page load notes
     *
     * @return void
     */
    public function testPageLoad()
    {
        $response = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'POST',
                '/api/',
                [
                    'note' => [
                        'page' => 1
                    ]
                ]
            );

        $response
            ->assertStatus(200)
            ->assertJson([
                'count' => true
            ]);
    }
}
