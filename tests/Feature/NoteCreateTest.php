<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NoteCreateTest extends TestCase
{
    /**
     * Test if note is created
     *
     * @return void
     */
    public function testCreateNote()
    {
        $response = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
            'POST',
                '/api/create',
                [
                    'note' => [
                        'title' => 'Test Title',
                        'content' => 'Test Content',
                        'category_id' => 0
                    ]
                ]
        );

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'title' => 'Test Title'
            ]);
    }
}
