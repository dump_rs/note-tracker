<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetCategoryNotesTest extends TestCase
{
    /**
     * Test list categories notes
     *
     * @return void
     */
    public function testCategoryLoadNotes()
    {
        $response = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'POST',
                '/api/category/list',[
                    'category' => [
                        'id' => 0
                    ]
                ]
            );

        $response
            ->assertStatus(200);
    }
}
