<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ColorNoteTest extends TestCase
{
    /**
     * Test color note
     *
     * @return void
     */
    public function testColorNote()
    {
        $response_create = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'POST',
                '/api/create',
                [
                    'note' => [
                        'title' => 'Test Color Title',
                        'content' => 'Test Color Content',
                        'category_id' => 0
                    ]
                ]
            );

        $response_create
            ->assertStatus(200)
            ->assertJson([
                'title' => true
            ]);

        $created_note = json_decode($response_create->getContent());

        $response = $this->withHeaders(['Content-Type' => 'application/json'])
            ->json(
                'PATCH',
                '/api/color',
                [
                    'note' => [
                        'id' => $created_note->id,
                        'color' => '#b7e9ff',
                        'category_id' => 0
                    ]
                ]
            );

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'color' => '#b7e9ff'
            ]);

    }
}
